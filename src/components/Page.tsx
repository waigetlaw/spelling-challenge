import React, { useEffect, useState } from "react";
import { Button, Card, Form, Spinner, Table } from "react-bootstrap";
import words from "../words";
import Speech from "speak-tts";
import axios from "axios";

import "./Page.css"
import { GraphUp, Check2Circle, X } from "react-bootstrap-icons";

const speech = new Speech();

speech.init({
    volume: 1,
    lang: "en-GB",
    rate: 1,
    patch: 1,
    voice: "Google UK English Female"
})

const Page: React.FC = props => {

    const [currentWord, setCurrentWord] = useState(getRandomWord());
    const [currentDef, setCurrentDef] = useState([]);
    const [spelling, setSpelling] = useState("");
    const [skipped, setSkipped] = useState(0);
    const [correct, setCorrect] = useState(0);
    const [wrong, setWrong] = useState(0);
    const [showAnswer, setShowAnswer] = useState(false);
    const [loading, setLoading] = useState(false);
    const [stats, setStats] = useState(false);

    const handleClose = (event: any) => {
        event.preventDefault();
        nextWord();
        setShowAnswer(false);
    }
    const handleShow = () => setShowAnswer(true);

    useEffect(() => {
        async function setDef(word: string) {
            setLoading(true);
            try {
                const result = await axios.get(`https://api.dictionaryapi.dev/api/v2/entries/en/${word}`, {
                });
                const re = new RegExp(word, 'gi');
                setCurrentDef(result.data[0].meanings[0].definitions.map(def => `[${def.partOfSpeech}] ${def.definition.replace(re, '*****')}`));

                const meanings = result.data[0].meanings.map((meaning: { partOfSpeech: string, definitions: { definition: string }[] }) => {
                    return meaning.definitions.map((def) => {
                        return `[${meaning.partOfSpeech}] ${def.definition.replace(re, '*****')}`;
                    });
                });

                const def = meanings.reduce((defs, meaning) => {
                    return [...defs, ...meaning]
                }, [])

                setCurrentDef(def);
            } catch (e) {
                try {
                    // try https://www.datamuse.com/api/
                    const result = await axios.get(`https://api.datamuse.com/words?sp=${word}&md=d`);
                    setCurrentDef(result.data[0].defs.map(def => def.split('\t')[1].replaceAll(word, '*****')))
                } catch (e) {
                    setCurrentDef([]);
                }
            } finally {
                setLoading(false);
                await speech.speak({ text: currentWord })
            }
        }
        setDef(currentWord);

    }, [currentWord])

    function submitSpelling(event: any) {
        event.preventDefault();
        if (spelling.toUpperCase() === currentWord.toUpperCase()) {
            setCorrect(correct + 1);
        } else {
            setWrong(wrong + 1);
        }
        handleShow();
    }

    function skip() {
        setSkipped(skipped + 1);
        nextWord();
    }

    function nextWord() {
        setSpelling("");
        setCurrentWord(getRandomWord())
    }

    async function sayWord() {
        await speech.speak({ text: currentWord })
    }

    function getRandomWord() {
        return words[Math.floor(Math.random() * words.length)];
    }

    function toggleStats() {
        setStats(!stats);
    }

    const questionCard = <Card.Body>
        {loading ? <Spinner animation="border" role="status">
            <span className="sr-only">Loading...</span>
        </Spinner> : <div>
                <Card.Title>Definition</Card.Title>
                {
                    currentDef.length ? currentDef.map((def, i) => (
                        <Card.Text key={i}>
                            {`${i + 1}) ${def}`}
                        </Card.Text>
                    )) : <Card.Text>No Definitions Found.</Card.Text>
                }
                <Button variant="info" onClick={sayWord}>Say Again</Button>
                <Form onSubmit={submitSpelling} className="form">
                    <Form.Group controlId="spelling" className="form-group">
                        <Form.Control autoComplete="off" value={spelling} onChange={e => setSpelling(e.target.value)} autoFocus />
                        <Button style={{ margin: '0 1rem' }} variant="success" type="submit" disabled={loading}>Submit</Button>
                        <Button variant="secondary" onClick={skip} disabled={loading}>Skip</Button>
                    </Form.Group>
                </Form>
            </div>}
    </Card.Body>

    const answerCard = <Card.Body>
        <Card.Title>{spelling.toUpperCase() === currentWord.toUpperCase() ? 'Correct!' : 'Wrong'}</Card.Title>
        {spelling.toUpperCase() === currentWord.toUpperCase() ?
            <div style={{ margin: "0 auto" }}>
                <div style={{ display: "flex", alignItems: "center", justifyContent: "center" }}>
                    {currentWord.toUpperCase()} < Check2Circle color="green" size={24} />
                </div>
            </div> :
            <div>
                <div style={{ display: "flex", alignItems: "center", justifyContent: "center" }}>
                    <div style={{ color: "red" }}>{spelling.toUpperCase()}</div> <X color="red" size={24} />
                </div>
                {currentWord.toUpperCase()}
            </div>
        }
        <Form onSubmit={handleClose}>
            <Button variant="primary" type="submit" autoFocus style={{ marginTop: "1rem" }}>
                Next
        </Button>
        </Form>
    </Card.Body>

    const statsCard = <Card.Body>
        <Card.Title>Stats</Card.Title>
        <Table bordered>
            <tbody>
                <tr>
                    <td>Skipped:</td>
                    <td>{skipped}</td>
                </tr>
                <tr>
                    <td>Correct:</td>
                    <td>{correct}</td>
                </tr>
                <tr>
                    <td>Wrong:</td>
                    <td>{wrong}</td>
                </tr>
                <tr>
                    <td>Total:</td>
                    <td>{skipped + wrong + correct}</td>
                </tr>
                <tr>
                    <td>Score:</td>
                    <td>{
                        ((skipped + wrong + correct) > 0 ?
                            Math.round((correct * 10000) / (skipped + wrong + correct)) / 100 : 0).toFixed(2)}%</td>
                </tr>
            </tbody>
        </Table>
    </Card.Body>

    return (
        <div style={{ margin: "1rem", minWidth: "1000px" }}>
            <h1>Spelling Practice</h1>
            <div className="page">
                <Card border="dark" bg="light" style={{ width: "500px" }}>
                    <div>
                        <Button style={{ float: "right", margin: "1rem", marginBottom: "0" }} onClick={toggleStats}><GraphUp /></Button>
                    </div>
                    {
                        stats ? statsCard :
                            <div style={{ margin: "auto" }}>
                                {showAnswer ? answerCard : questionCard}
                            </div>
                    }

                </Card>
            </div>
            <div className="footer">
                <span style={{ float: "left" }}>© Wai Get Law</span>
                <span style={{ float: "right" }}>Code available at: <a rel="noreferrer" href="https://gitlab.com/waigetlaw/spelling-challenge" target="_blank">Gitlab</a></span>
            </div>
        </div>
    )
}

export default Page;

